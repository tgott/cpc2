# Computer Programming 2

Computer lab templates for BPC-PP2, BKC-PP2 and CPC2 courses at Brno University of Technology. The codes are written in Python 3. If you have any questions about my solutions, email me at gotthans@vutbr.cz
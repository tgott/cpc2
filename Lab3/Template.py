# -*- coding: utf-8 -*-
"""
-----------------------------------------------------------------------------
Computer and Programming 2, course @ Brno University of Technology
Lab 3, Assigment 1-3: Do letters frequency analysis on loaded file and encrypt
another loaded file with Caesar cypher. Then do frequency analysis to
estimate shift.
-----------------------------------------------------------------------------
Author: Tomas Gotthans, gotthans@vutbr.cz
"""

import requests
import unicodedata


def strip_accents(text):
    # script for removing characters from string such as []()<> ...
    try:
        text = unicode(text, 'utf-8')
    except (TypeError, NameError):
        pass
    text = unicodedata.normalize('NFD', text)
    text = text.encode('ascii', 'ignore')
    text = text.decode("utf-8")
    permitted = "[]()<>?.,!/;: 1234567890*-=#"
    for i in range(0, len(permitted)):
        text = text.replace(permitted[i], "")
    text = text.replace("\r", "")
    text = text.replace("\n", "")
    text = text.replace("\'", "")
    text = text.replace("\"", "")
    text = text.lower()
    return str(text)

def arrange_dictionary(myDictionary):
# INPUT myDictionary={"two":"2", "one":"1", "five":"5", "four":"4"}
# OUTPUT newDictionary={'one': '1', 'two': '2', 'four': '4', 'five': '5'}
    newDictionary={}
    sortedList=sorted(myDictionary.values())
    for sortedKey in sortedList:
        for key, value in myDictionary.items():
            if value==sortedKey:
                newDictionary[key]=value
    return newDictionary

def char_frequency(str1):
    str1 = strip_accents(str1)
    """
      Code here
    """
    return arrange_dictionary(dict)


def caesar(plainText, shift):
      plainText = strip_accents(plainText)
      """
        Code here
      """
      return cipherText
